package telegram

import (
	"log"
	"strings"
)

const (
	GoidaCmd = "/goida"
	HelpCmd  = "/help"
	StartCmd = "/start"
)

func (ep EventProcessor) doCmd(text string, chatId int, username string) error {
	text = strings.TrimSpace(text)

	log.Printf("Got new command '%s' from %s", text, username)

	switch text {
	case GoidaCmd:
		return ep.sendGoida(chatId)
	case HelpCmd:
		return ep.sendHelp(chatId)
	case StartCmd:
		return ep.sendHello(chatId)
	default:
		return ep.tg.SendMessage(chatId, msgUnknownCommand)
	}
}

func (ep *EventProcessor) sendHelp(chatId int) error {
	return ep.tg.SendMessage(chatId, msgHelp)
}

func (ep *EventProcessor) sendGoida(chatId int) error {
	return ep.tg.SendMessage(chatId, msgGoida)
}

func (ep *EventProcessor) sendHello(chatId int) error {
	return ep.tg.SendMessage(chatId, msgHello)
}
